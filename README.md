Shop giày 2hand (secondhand) KINGSMAN chuyên kinh doanh các mặt hàng giày da cho nam giới, là cấu nối cho bạn trẻ cả nước tiếp cận với xu hướng thời trang giày trên thế giới. “Chúng tôi mang đến sự khác biệt!” – Kingsman đã và đang không ngừng đổi mới để mang đến cho các đấng mày râu những đôi giày chất lượng, độc đáo và thời trang từ những thương hiệu giày hàng đầu thế giới với giá chỉ bằng 1/10 so với giày mới chính hãng nhập khẩu. 

KINGSMAN đang dần trở thành một cái tên quen thuộc với tất cả các bạn trẻ với những phong cách thời trang hoàn toàn mới lạ, khẳng định cá tính của chính mình.

Địa chỉ: 59/66/3 Mã Lò, Bình Trị Đông A, Bình Tân, Hồ Chí Minh

Điện thoại: 0906919433

Email: giaynamsecondhand@gmail.com

Các Website trực thuộc:
<a href=”https://shopgiay2hand.com/“>Giày 2hand</a>
<a href=”https://giayda2hand.com/“>Giày Secondhand</a>
<a href=”https://giaynamsecondhand.com/“>Shop Giày 2hand</a>